import React, { useEffect } from 'react'
import io from 'socket.io-client';

const socket = io('http://localhost:8000');

export default function Check() {
  useEffect(() => {
    console.log(socket)
    socket.on('connection', () => {
      console.log('connected')
    })
  }, []);
  return (
    <div>Check</div>
  )
}

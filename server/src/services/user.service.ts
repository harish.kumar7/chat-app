import { UserModel } from "../models/user.model";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { jwtSecret } from "../config";
import { ReqUser, User } from "../interfaces";

export const getUserOfMail = async (email: string) => {
  return await UserModel.findOne({ email });
};

export const createUser = async (
  email: string,
  name: string,
  password: string
) => {
  const user = new UserModel({
    name,
    email,
  });
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(password, salt);
  await user.save();
  return user;
};

export const createToken = (user: ReqUser) => {
  const payload = {
    user,
  };
  return jwt.sign(payload, jwtSecret, { expiresIn: "5 days" });
};

export const comparePassword = async (password: string, user: User) => {
  return await bcrypt.compare(password, user.password);
};

export const getUserOfId = async (_id: string) => {
  return await UserModel.findById(_id).select("-password -__v -joiningDate");
};

export const isEmail = (value: string) => {
  const regexExp =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/gi;
  return regexExp.test(value);
};

export const getUserByName = async (name: string) => {
  return await UserModel.findOne({ name: name }).select(
    "-password -__v -joiningDate"
  );
};

export const getUserByEmail = async (email: string) => {
  return await UserModel.findOne({ email: email }).select(
    "-password -__v -joiningDate"
  );
};

import express, { Express, json } from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
import loginRouter from './routes/auth.route'
import userRouter from './routes/user.route'
import { handleValidationError, handleMongooseError, handleDatabaseError, handleDefaultError } from './middlewares/errorHandler.middleware'
import path from 'path'

dotenv.config()
const app: Express = express()

if (process.env.NODE_ENV === 'development')
  app.use(cors())

// init middleware for express validator to be able to intercept request
app.use(json())

app.use('/api/user', userRouter)
app.use('/api/auth', loginRouter)

// error handlers
app.use(handleValidationError)
app.use(handleMongooseError)
app.use(handleDatabaseError)
app.use(handleDefaultError)

// serve build of frontend in express
app.use(express.static('../client/dist'))

app.get('*', (_req, res) => {
  res.sendFile(path.resolve('../client/dist/index.html'))
})

export default app

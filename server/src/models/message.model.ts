import { Schema, model } from "mongoose";
import { Message } from "../interfaces";

export const messageSchema = new Schema<Message>({
  content: {
    type: String,
    requried: true,
  },
  senderId: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  chatId: {
    type: Schema.Types.ObjectId,
    ref: "chat",
    required: true,
  },
  sent: {
    type: Boolean,
    requried: true,
  },
  deliveredTo: [{ type: Schema.Types.ObjectId, ref: "user", required: true }],
  readBy: [{ type: Schema.Types.ObjectId, ref: "user", required: true }],
});

export const MessageModel = model<Message>("message", messageSchema);

import app from "./app.js"
import { port } from "./config"
import { connectDB } from "./config/db"
import { Server } from 'socket.io'

connectDB()
  .then(() => {
    const server = app.listen(port, () => {
      console.log(`[server]: Server is running at PORT:${port}`)
    })
    

    const io = new Server(server, {
      pingTimeout: 60000,
      cors: {
        origin: "http://127.0.0.1:5173",
      }
    })

    io.on('connection', (socket) => {
      console.log('a user connected');
    });
  })
  .catch((err) => {
    console.error(err)
    process.exit(1)
  })
